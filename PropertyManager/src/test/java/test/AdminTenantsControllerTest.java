package test;

import org.junit.jupiter.api.Test;
import propertymanager.AdminTenantsController;

import static org.junit.jupiter.api.Assertions.*;

class AdminTenantsControllerTest {

	@Test
	void phoneNumOnlyDigits() {
		AdminTenantsController controller = new AdminTenantsController();
		String[] inputs = new String[]{
				"123-456-7890",
				"(123) 456-7890",
				"123 456 7890",
				"123.456.7890",
				"+1 (123) 456-7890"
		};
		String[] expected = new String[]{
				"1234567890",
				"1234567890",
				"1234567890",
				"1234567890",
				"11234567890"
		};
		for (int i = 0; i < expected.length; i++) {
			assertEquals(expected[i], controller.phoneNumOnlyDigits(inputs[i]));
		}
	}
}