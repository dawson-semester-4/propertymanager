package test;

import propertymanager.backend.*;

import static org.junit.jupiter.api.Assertions.*;

class PropertyFactoryTest {

	@org.junit.jupiter.api.Test
	void testGetPlex() {
		String address = "test address";
		String price = "1000";
		String unitCount = "2";
		Property p1 = PropertyFactory.getProperty(-1, address, "Plex", unitCount, price, null);
		Property p2 = new Plex(-1, address, unitCount, price);
		assertNotNull(p1);
		// I had to either use getters or overwrite the .equals() method to check for equality
		assertEquals(p1.getAddress(), p2.getAddress());
		assertEquals(p1.getType(), p2.getType());
		assertEquals(p1.getPrice(), p2.getPrice());
		assertEquals(p1.getUnitCount(), p2.getUnitCount());
	}

	@org.junit.jupiter.api.Test
	void testGetHouse() {
		String address = "test address";
		String price = "1000";
		Property p1 = PropertyFactory.getProperty(-1, address, "House", "1", price, null);
		Property p2 = new House(-1, address, price);
		assertNotNull(p1);
		assertEquals(p1.getAddress(), p2.getAddress());
		assertEquals(p1.getType(), p2.getType());
		assertEquals(p1.getPrice(), p2.getPrice());
		assertEquals(p1.getUnitCount(), p2.getUnitCount());
	}

	@org.junit.jupiter.api.Test
	void testGetCondo() {
		String address = "test address";
		String price = "1000";
		String condoFee = "100";
		Property p1 = PropertyFactory.getProperty(-1, address, "Condo", "1", price, condoFee);
		Property p2 = new Condo(-1, address, price, condoFee);
		assertNotNull(p1);
		assertEquals(p1.getAddress(), p2.getAddress());
		assertEquals(p1.getType(), p2.getType());
		assertEquals(p1.getPrice(), p2.getPrice());
		assertEquals(p1.getUnitCount(), p2.getUnitCount());
	}


}