package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.*;

import java.sql.Date;

public class AdminMaintenanceLogController extends Controller {

	@FXML
	private CheckBox editable;
	@FXML
	private TableView<MaintenanceLog> table;
	@FXML
	private TableColumn<MaintenanceLog, String> contractorID;
	@FXML
	private TableColumn<MaintenanceLog, String> address;
	@FXML
	private TableColumn<MaintenanceLog, String> apt;
	@FXML
	private TableColumn<MaintenanceLog, String> date;
	@FXML
	private TableColumn<MaintenanceLog, String> type;
	@FXML
	private TableColumn<MaintenanceLog, String> description;

	@FXML
	private HBox addDetails;

	@FXML
	private VBox actionButtons;

	@FXML
	private TextField contractorIDInput;

	@FXML
	private TextField addressInput;

	@FXML
	private TextField unitInput;

	@FXML
	private DatePicker dateInput;

	@FXML
	private TextField typeInput;

	@FXML
	private TextField descriptionInput;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		contractorID.setCellValueFactory(new PropertyValueFactory<>("contractorID"));
		initColumn(contractorID);
		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		initColumn(address);
		apt.setCellValueFactory(new PropertyValueFactory<>("aptNum"));
		initColumn(apt);
		date.setCellValueFactory(new PropertyValueFactory<>("date"));
		initColumn(date);
		type.setCellValueFactory(new PropertyValueFactory<>("type"));
		initColumn(type);
		description.setCellValueFactory(new PropertyValueFactory<>("description"));
		initColumn(description);

		table.getItems().addAll(MaintenanceLog.getLog());
	}

	private void initColumn(TableColumn<MaintenanceLog, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getUnitID();
			MaintenanceLog.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	@FXML
	private void addLog() {
		try {
			MaintenanceLog.addLog(Integer.parseInt(contractorIDInput.getText().trim()), addressInput.getText().trim(),
			                      Integer.parseInt(unitInput.getText().trim()), Date.valueOf(dateInput.getValue()),
			                      typeInput.getText().trim(),
			                      descriptionInput.getText().trim());
		} catch (NumberFormatException | NullPointerException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Input");
			alert.setContentText("One or more inputs are invalid!");
			alert.showAndWait();
			return;
		}
		refresh();
	}

	@FXML
	private void deleteSelectedRow() {
		MaintenanceLog.removeLog(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		table.getItems().addAll(MaintenanceLog.getLog());
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}
