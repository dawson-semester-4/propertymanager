package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import propertymanager.backend.Login;

public class LoginController extends Controller {

	private static String nameGiven = "";
	private static int unitGiven;

	@FXML
	private VBox details;
	@FXML
	private TextField name;
	@FXML
	private TextField unit;
	@FXML
	private PasswordField password;

	@FXML
	private void initialize() {
		details.addEventHandler(KeyEvent.KEY_PRESSED, keyEvent -> {
			if (keyEvent.getCode() == KeyCode.ENTER) {
				login();
			}
		});
	}

	public static String getNameGiven() {
		return nameGiven;
	}

	public static int getUnitGiven() {
		return unitGiven;
	}

	@FXML
	private void login() {
		nameGiven = name.getText().trim();
		try {
			unitGiven = Integer.parseInt(unit.getText().trim());
		} catch (NumberFormatException e) {
			unitGiven = -1;
		}
		String passwordGiven = password.getText();

		int loginResult = Login.login(nameGiven, unitGiven, passwordGiven);
		switch (loginResult) {
			case -1 -> {
				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Error");
				alert.setHeaderText("User Unknown");
				alert.setContentText("You have to register that user before logging in!");
				alert.showAndWait().ifPresent(rs -> {
					if (rs == ButtonType.OK) {
						name.clear();
						unit.clear();
						password.clear();
					}
				});
			}
			case 0 -> {
				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Error");
				alert.setHeaderText("Name or password incorrect");
				alert.setContentText("Try a different name or password, or contact the administrator");
				alert.showAndWait().ifPresent(rs -> {
					if (rs == ButtonType.OK) {
						password.clear();
					}
				});
			}
			case 1 -> App.setScene("tenant_home");
			case 2 -> App.setScene("admin_home");
		}
	}

	@FXML
	private void switchToRegistration() {
		App.setScene("registration");
	}

	@FXML
	private void loginTest() {
		App.setScene("tenant_home");
	}

	void refresh() {}
}
