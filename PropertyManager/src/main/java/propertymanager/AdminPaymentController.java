package propertymanager;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableBooleanValue;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.StringConverter;
import propertymanager.backend.*;

import java.sql.Date;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.EventListener;

public class AdminPaymentController extends Controller {
	@FXML
	private CheckBox editable;
	@FXML
	private TableView<Payment> table;
	@FXML
	private TableColumn<Payment, Boolean> paid;
	@FXML
	private TableColumn<Payment, String> paymentDueDate;
	@FXML
	private TableColumn<Payment, String> datePaid;
	@FXML
	private TableColumn<Payment, String> paymentMethod;
	@FXML
	private TableColumn<Payment, String> amount;

	@FXML
	private HBox addDetails;

	@FXML
	private VBox actionButtons;

	@FXML
	private Label inputInfo;

	@FXML
	private TextField propertyAddressInput;

	@FXML
	private TextField unitInput;

	@FXML
	private TextField paidInput;

	@FXML
	private DatePicker paymentDueDateInput;

	@FXML
	private DatePicker datePaidInput;

	@FXML
	private ComboBox<String> paymentMethodInput;

	@FXML
	private TextField amountInput;

	private static Property propertyUsed;
	private static Unit unitUsed;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		paid.setCellValueFactory(p -> p.getValue().getPaidProperty());
		paid.setCellFactory((TableColumn<Payment, Boolean> p) -> {
			final CheckBoxTableCell<Payment, Boolean> checkbox = new CheckBoxTableCell<>();
			// created BooleanProperty because the checkbox value itself cannot be observed, but a boolean can
			final BooleanProperty checked = new SimpleBooleanProperty();
			// initialize the value of the checkbox
			checkbox.setConverter(new StringConverter<>() {
				                      @Override
				                      public String toString(Boolean bool) {
					                      return bool.toString();
				                      }

				                      @Override
				                      public Boolean fromString(String s) {
					                      return s != null;
				                      }
			                      }
			);
			checkbox.setSelectedStateCallback(index -> {
				boolean hasPaid = paid.getCellObservableValue(index).getValue();
				checked.setValue(hasPaid);
				checkbox.setText(hasPaid ? "Yes" : "No ");
				return paid.getCellObservableValue(index);
			});
			checked.addListener((obs, oldVal, newVal) -> {
				// update table when the BooleanProperty, checked, is changed
				int recordIdentifier = checkbox.getTableRow().getItem().getUnitID();
				Payment.update(checkbox.getTableColumn().getText(), recordIdentifier, newVal ? "1" : "0");
			});
			// returns to setCellFactory
			return checkbox;
		});
		paymentDueDate.setCellValueFactory(new PropertyValueFactory<>("paymentDueDate"));
		initColumn(paymentDueDate);
		datePaid.setCellValueFactory(new PropertyValueFactory<>("datePaid"));
		initColumn(datePaid);
		paymentMethod.setCellValueFactory(new PropertyValueFactory<>("paymentMethod"));
		initColumn(paymentMethod);
		amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
		initColumn(amount);

		paymentMethodInput.getItems().addAll("Cheque", "Cash", "Debit", "Mastercard", "Visa");
		paymentMethodInput.getSelectionModel().selectFirst();

		propertyAddressInput.textProperty().addListener((obs, oldText, newText) -> {
			propertyUsed = Property.getProperty(newText);
			unitInput.setDisable(!(propertyUsed instanceof Plex));

			// put only numbers from the address into the apt number input
			String address = propertyAddressInput.getText().trim();
			unitInput.setText(onlyDigits(address));
			searchUnit();
			refresh();
		});

		unitInput.textProperty().addListener((obs, oldText, newText) -> {
			unitUsed = Unit.getUnit(propertyAddressInput.getText().trim(), unitInput.getText().trim());
			table.getItems().addAll(Payment.getPayment(unitUsed.getUnitID()));
		});


		paidInput.textProperty().addListener((obs, oldText, newText) -> {
			if (validatePaidInput() && hasPaid()) {
				datePaidInput.setDisable(false);
				paymentMethodInput.setDisable(false);
				amountInput.setDisable(false);
			} else {
				datePaidInput.setDisable(true);
				paymentMethodInput.setDisable(true);
				amountInput.setDisable(true);
			}
		});
	}

	private void initColumn(TableColumn<Payment, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getUnitID();
			Payment.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	private void searchUnit() {
		unitUsed = Unit.getUnit(propertyAddressInput.getText().trim(), unitInput.getText().trim());
		inputInfo.setVisible(unitUsed != null);
	}

	@FXML
	private void addPayment() {
		if (!validatePaidInput()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid paid Input");
			alert.setContentText("Has paid entered incorrectly!");
			alert.showAndWait();
			return;
		}

		float amountNum;
		try {
			amountNum = Float.parseFloat(amountInput.getText());
		} catch (NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid amount paid Input");
			alert.setContentText("Amount paid entered incorrectly!");
			alert.showAndWait();
			return;
		}

		try {
			Payment.addPayment(unitUsed.getUnitID(),
			                   hasPaid(),
			                   Date.valueOf(paymentDueDateInput.getValue()),
			                   Date.valueOf(datePaidInput.getValue()),
			                   paymentMethodInput.getValue(), amountNum);
		} catch (SQLIntegrityConstraintViolationException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Tenant does not exist");
			alert.setContentText("A tenant must be added before trying to make a payment");
			alert.showAndWait();
			return;
		} catch (NullPointerException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("No property selected");
			alert.setContentText("Enter an address to add a payment to it");
			alert.showAndWait();
			return;
		}
		refresh();
	}

	private boolean hasPaid() {
		return paidInput.getText().charAt(0) == 'y';
	}

	private boolean validatePaidInput() {
		return paidInput.getText().equalsIgnoreCase("yes") ||
				paidInput.getText().equalsIgnoreCase("no") ||
				(paidInput.getText().length() == 1 && paidInput.getText().charAt(0) == 'y') ||
				(paidInput.getText().length() == 1 && paidInput.getText().charAt(0) == 'n');
	}

	@FXML
	private void deleteSelectedRow() {
		Payment.removePayment(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		table.getItems().addAll(Payment.getPayment(unitUsed.getUnitID()));
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}