package propertymanager;

import javafx.fxml.FXML;

public class TenantController {

	@FXML
	private void switchToLogin() {
		App.setScene("login");
	}

	@FXML
	private void switchToLease() {
		App.setScene("tenant_lease");
	}

	@FXML
	private void switchToPayments() {
		App.setScene("tenant_payment");
	}
}
