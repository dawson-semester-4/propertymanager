package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import propertymanager.backend.Registration;

public class RegistrationController extends Controller {
	@FXML
	private TextField name;
	@FXML
	private TextField address;
	@FXML
	private TextField unit;
	@FXML
	private PasswordField password;

	@FXML
	private void register() {
		int unitNum;
		try {
			unitNum = Integer.parseInt(unit.getText().trim());
		} catch (NumberFormatException e) {
			unitNum = -1;
		}
		Registration.register(name.getText().trim(), address.getText().trim(), unitNum, password.getText().trim());

		// switch back to login
		switchToPrevious();
	}

	void refresh() {}
}