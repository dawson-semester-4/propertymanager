package propertymanager.backend;

public class Condo extends Property {
	private final String condoFee;

	public Condo(int propertyID, String address, String price, String condoFee) {
		super(propertyID, address, "Condo", "1", price);
		this.condoFee = condoFee;
	}

	public String getCondoFee() {
		return condoFee;
	}
}
