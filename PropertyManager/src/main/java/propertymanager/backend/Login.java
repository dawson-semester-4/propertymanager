package propertymanager.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Login extends Security {
	private static String name = "";

	public static String getName() {
		return name;
	}

	public static int login(String name, int aptNum, String password) {
		if (!userExists(name, aptNum))
			return -1;
		if (checkCorrectPassword(name, password)) {
			Login.name = name;
			if (aptNum == -1)
				return 2; // if the user is an admin, login as admin (there will be a better implementation in the future)
			return 1; // if the user is not an admin, login as tenant
		}
		return 0; // if the password is incorrect
	}

	private static boolean checkCorrectPassword(String name, String password) {
		if (name == null || password == null || name.isEmpty() || password.isEmpty())
			return false;

		// get salt from Login table
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String getSalt = "select salt from login where name = ?";
			PreparedStatement ps = conn.prepareStatement(getSalt);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			rs.next();
			byte[] salt = rs.getString("salt").getBytes();

			String newHashedPassword = hashPassword(password, salt);

			// get hashedPassword from Login table
			String getPassword = "select password_hash from login where name = ?";
			PreparedStatement ps2 = conn.prepareStatement(getPassword);
			ps2.setString(1, name);
			ResultSet rs2 = ps2.executeQuery();
			rs2.next();
			String storedHashedPassword = rs2.getString("password_hash");

			if (newHashedPassword.equals(storedHashedPassword)) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
