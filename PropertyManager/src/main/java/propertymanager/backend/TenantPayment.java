package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TenantPayment extends Payment {
	private final String address;
	private final String aptNum;

	public TenantPayment(int unitID, String address, String aptNum, boolean paid, String paymentDueDate, String datePaid, String paymentMethod, String amount) {
		super(unitID, paid, paymentDueDate, datePaid, paymentMethod, amount);
		this.address = address;
		this.aptNum = aptNum;
	}

//	private final int unitID;
//	private final String address;
//	private final String aptNum;
//	private final String paymentDueDate;
//	private final String datePaid;
//	private final String paymentMethod;
//	private final String amount;
//
//	public TenantPayment(int unitID, String address, String aptNum, String paymentDueDate, String datePaid, String paymentMethod, String amount) {
//		this.unitID = unitID;
//		this.address = address;
//		this.aptNum = aptNum;
//		this.paymentDueDate = paymentDueDate;
//		this.datePaid = datePaid;
//		this.paymentMethod = paymentMethod;
//		this.amount = amount;
//	}
//
//	public int getUnitID() {
//		return unitID;
//	}
//
//	public String getAddress() {
//		return address;
//	}
//
//	public String getAptNum() {
//		return aptNum;
//	}
//
//	public String getPaymentDueDate() {
//		return paymentDueDate;
//	}
//
//	public String getDatePaid() {
//		return datePaid;
//	}
//
//	public String getPaymentMethod() {
//		return paymentMethod;
//	}
//
//	public String getAmount() {
//		return amount;
//	}

	public static List<TenantPayment> getTenantPayments(String name, int aptNum) throws IllegalArgumentException {
		List<TenantPayment> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			// get unitID from tables
			String sql = "select t.unit_id " +
					"from tenants t " +
					"left join units u on t.unit_id = u.unit_id " +
					"where t.name = ? and u.apt_number = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, aptNum);
			ResultSet rs = ps.executeQuery();
			int unitID;
			if (rs.next() && rs.getFetchSize() == 1) {
				unitID = rs.getInt(1);
			}
			else {
				throw new IllegalArgumentException("Tenant does not exist");
			}

			// get tenant payment
			String sql2 = "select p.unit_id, p2.address, u.apt_number, paid_flag, payment_due_date, date_paid, payment_method, amount " +
					"from payment p " +
					"left join units u on p.unit_id = u.unit_id " +
					"left join properties p2 on u.property_id = p2.property_id " +
					"where p.unit_id = ?" +
					"group by p.unit_id";
			ps = conn.prepareStatement(sql2);
			ps.setInt(1, unitID);
			rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new TenantPayment(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getBoolean(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7),
						rs.getString(8)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}
}
