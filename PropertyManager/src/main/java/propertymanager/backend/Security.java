package propertymanager.backend;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.sql.*;
import java.util.Base64;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class Security {

	public static int getUnitID(int aptNum) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select unit_id from units where apt_number = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, aptNum);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static boolean userExists(String name, int aptNum) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String getUser;
			if (aptNum == -1)
				getUser = "select name from login where name = ? and unit_id is null";
			else
				getUser = "select name from login " +
						"where name = ? and unit_id = (select unit_id from units where apt_number = ?)";
			PreparedStatement ps = conn.prepareStatement(getUser);
			ps.setString(1, name);
			if (aptNum != -1)
				ps.setInt(2, aptNum);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				// if the user specified is in the DB, return true
				if (name.equalsIgnoreCase(rs.getString("name")))
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	protected static byte[] generateSalt() {
		return new BigInteger(130, new SecureRandom()).toString().getBytes();
	}

	protected static String hashPassword(String password, byte[] salt) {
		if (password == null || password.isEmpty() || salt == null)
			return null;

		try {
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 512, 128);
			SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = f.generateSecret(spec).getEncoded();
			Base64.Encoder encoder = Base64.getEncoder();
			// System.out.println("salt: " + new String(salt));
			// System.out.println("hash: " + encoder.encodeToString(hash));
			return encoder.encodeToString(hash);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
