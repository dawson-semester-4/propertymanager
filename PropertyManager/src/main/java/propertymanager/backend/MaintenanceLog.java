package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MaintenanceLog {
	private final int unitID;
	private final int contractorID;
	private final String address;
	private final String aptNum;
	private final String date;
	private final String type;
	private final String description;

	public MaintenanceLog(int unitID, int contractorID, String address, String apt, String date, String type, String description) {
		this.unitID = unitID;
		this.contractorID = contractorID;
		this.address = address;
		this.aptNum = apt;
		this.date = date;
		this.type = type;
		this.description = description;
	}

	public int getUnitID() {
		return unitID;
	}

	public String getContractorID() {
		return String.valueOf(contractorID);
	}

	public String getAddress() {
		return address;
	}

	public String getAptNum() {
		return aptNum;
	}

	public String getDate() {
		return date;
	}

	public String getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

	public static void update(String field, int logDiscovery, String newValue) {
		switch (field) {
			case "Date" -> field = "date";
			case "Type" -> field = "type";
			case "Description" -> field = "description";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update maintenance_log set " + field + " = ? where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, logDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addLog(int contractorID, String address, int apt, Date date, String type, String description) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select unit_id from units u where u.property_id = (select property_id from properties where address like ?) and apt_number = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, address + "%");
			ps.setInt(2, apt);
			ResultSet rs = ps.executeQuery();
			int unitID = -1;
			if (rs.next()) {
				unitID = rs.getInt(1);
			}

			String sql2 = "insert into maintenance_log(unit_id, contractor_id, date, type, description) values(?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(sql2);
			ps.setInt(1, unitID);
			ps.setInt(2, contractorID);
			ps.setDate(3, date);
			ps.setString(4, type);
			ps.setString(5, description);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<MaintenanceLog> getLog() {
		// Creating 2d array to store query
		List<MaintenanceLog> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select u.unit_id, l.contractor_id, p.address, u.apt_number, l.date, l.type, l.description " +
					"from maintenance_log l " +
					"left join units u on l.unit_id = u.unit_id " +
					"left join properties p on u.property_id = p.property_id " +
					"group by l.contractor_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new MaintenanceLog(
						rs.getInt(1),
						rs.getInt(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeLog(MaintenanceLog toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from maintenance_log where unit_id = ? and contractor_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, toRemove.getUnitID());
			ps.setString(2, toRemove.getContractorID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
