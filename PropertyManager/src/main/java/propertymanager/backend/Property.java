package propertymanager.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

// Factory pattern
public abstract class Property {
	private final int propertyID;
	private final String address;
	private final String type;
	private final String unitCount;
	private final String price;

	public Property(int propertyID, String address, String type, String unitCount, String price) {
		this.propertyID = propertyID;
		this.address = address;
		this.type = type;
		this.unitCount = unitCount;
		this.price = price;
	}

	public int getPropertyID() {
		return propertyID;
	}

	public String getAddress() {
		return address;
	}

	public String getType() {
		return type;
	}

	public String getUnitCount() {
		return unitCount;
	}

	public String getPrice() {
		return price;
	}

	public static void update(String field, String addressDiscovery, String newValue) {
		switch (field) {
			case "Property Address" -> field = "address";
			case "Type" -> field = "type";
			case "Unit count" -> field = "unit_count";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update properties set " + field + " = ? where address = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setString(2, addressDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addProperty(String address, String type, int unitCount) throws IllegalArgumentException {
		if (address.isEmpty() || type.isEmpty() || unitCount == -1)
			throw new IllegalArgumentException("Invalid Input");

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "insert into properties(type, address, unit_count) values(?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, type);
			ps.setString(2, address);
			ps.setInt(3, unitCount);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Property> getProperties() {
		// Creating 2d array to store query
		List<Property> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select p.property_id, p.address, p.type, p.unit_count, coalesce(m.price, 'n/a'), u.condo_fee " +
					"from properties p " +
					"left join mortgage m on p.property_id = m.property_id " +
					"left join units u on p.property_id = u.property_id " +
					"group by p.property_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(PropertyFactory.getProperty(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeProperty(Property toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from properties where address = ? and type = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, toRemove.getAddress());
			ps.setString(2, toRemove.getType());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Property getProperty(String address) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select p.property_id, p.address, p.type, p.unit_count, coalesce(m.price, 'n/a'), u.condo_fee " +
					"from properties p " +
					"left join mortgage m on p.property_id = m.property_id " +
					"left join units u on p.property_id = u.property_id " +
					"where p.address like ? " +
					"group by p.property_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, address + "%");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return PropertyFactory.getProperty(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
