package propertymanager.backend;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Payment {
	private final int unitID;
	private final BooleanProperty paid = new SimpleBooleanProperty();
	private final String paymentDueDate;
	private final String datePaid;
	private final String paymentMethod;
	private final String amount;

	public Payment(int unitID, boolean paid, String paymentDueDate, String datePaid, String paymentMethod, String amount) {
		this.unitID = unitID;
		this.paid.set(paid);
		this.paymentDueDate = paymentDueDate;
		this.datePaid = datePaid;
		this.paymentMethod = paymentMethod;
		this.amount = amount;
	}

	public int getUnitID() {
		return unitID;
	}

	public BooleanProperty getPaidProperty() {
		return paid;
	}

	public boolean getPaid() {
		return paid.get();
	}

	public String getPaymentDueDate() {
		return paymentDueDate;
	}

	public String getDatePaid() {
		return datePaid;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public String getAmount() {
		return amount;
	}

	public static void update(String field, int unitDiscovery, String newValue) {
		switch (field) {
			case "Paid" -> field = "paid_flag";
			case "Payment Due" -> field = "payment_due_date";
			case "Date Paid" -> field = "date_paid";
			case "Payment Method" -> field = "payment_method";
			case "Amount" -> field = "amount";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update payment set " + field + " = ? where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, unitDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addPayment(int unitID, boolean paid, Date paymentDueDate, Date datePaid, String paymentMethod, float amount) throws SQLIntegrityConstraintViolationException {
//        if (unitID == -1 || amount == -1 || paymentDueDate == null)
//            throw new IllegalArgumentException("Invalid Input");

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "insert into payment(unit_id, paid_flag, payment_due_date, date_paid, payment_method, amount) values(?, ?, ?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ps.setBoolean(2, paid);
			ps.setDate(3, paymentDueDate);
			if (paid) {
				ps.setDate(4, datePaid);
				ps.setString(5, paymentMethod);
				ps.setFloat(6, amount);
			} else {
				ps.setNull(4, Types.DATE);
				ps.setNull(5, Types.VARCHAR);
				ps.setNull(6, Types.DECIMAL);
			}

			System.out.println("sql = " + sql);
			System.out.println(
					"unitID = " + unitID + ", paid = " + paid + ", paymentDueDate = " + paymentDueDate +
							", datePaid = " + datePaid + ", paymentMethod = " + paymentMethod + ", amount = " + amount);

			ps.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new SQLIntegrityConstraintViolationException();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Payment> getPayment(int unitID) {
		List<Payment> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select unit_id, paid_flag, payment_due_date, date_paid, payment_method, amount " +
					"from payment " +
					"where unit_id = ? " +
					"group by unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Payment(
						rs.getInt(1),
						rs.getBoolean(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static List<Payment> getPayments() {
		List<Payment> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select unit_id, paid_flag, payment_due_date, date_paid, payment_method, amount " +
					"from payment " +
					"group by unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Payment(
						rs.getInt(1),
						rs.getBoolean(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removePayment(Payment toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from payment where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, toRemove.getUnitID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
