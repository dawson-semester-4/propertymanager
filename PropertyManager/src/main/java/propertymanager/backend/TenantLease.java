package propertymanager.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TenantLease extends Lease {
	public TenantLease(int unitID, String name, String address, String unit, String startDate, String endDate, String monthlyRent) {
		super(unitID, name, address, unit, startDate, endDate, monthlyRent);
	}

	public static List<TenantLease> getTenantLease(String name, int aptNum) throws IllegalArgumentException {
		List<TenantLease> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			// get unitID from tables
			String sql = "select t.unit_id " +
					"from tenants t " +
					"left join units u on t.unit_id = u.unit_id " +
					"where t.name = ? and u.apt_number = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, aptNum);
			ResultSet rs = ps.executeQuery();
			int unitID;
			if (rs.next() && rs.getFetchSize() == 1) {
				unitID = rs.getInt(1);
			}
			else {
				throw new IllegalArgumentException("Lease does not exist");
			}

			// get tenant lease
			String sql2 = "select l.unit_id, t.name, p.address, u.apt_number, l.start_date, l.end_date, l.rent " +
					"from leases l " +
					"left join tenants t on l.unit_id = t.unit_id " +
					"left join units u on t.unit_id = u.unit_id " +
					"left join properties p on u.property_id = p.property_id " +
					"where u.unit_id = ? " +
					"group by l.unit_id";
			ps = conn.prepareStatement(sql2);
			ps.setInt(1, unitID);
			rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new TenantLease(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}
}
