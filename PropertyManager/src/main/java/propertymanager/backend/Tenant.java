package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Tenant {
	private final int unitID;
	private final String name;
	private final String address;
	private final String aptNum;
	private final String email;
	private final String phone;

	public Tenant(int unitID, String name, String address, String aptNum, String email, String phone) {
		this.unitID = unitID;
		this.name = name;
		this.address = address;
		this.aptNum = aptNum;
		this.email = email;
		this.phone = phone;
	}

	public int getUnitID() {
		return unitID;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getAptNum() {
		return aptNum;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public static void update(String field, int unitDiscovery, String newValue) {
		switch (field) {
			case "Start Date" -> field = "start_date";
			case "End Date" -> field = "end_date";
			case "Monthly Rent" -> field = "rent";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update tenants set " + field + " = ? where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, unitDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addTenant(int unitID, String name, String email, String phone) throws IllegalArgumentException {
		if (unitID == -1 || name.isEmpty() || email.isEmpty() || phone.isEmpty())
			throw new IllegalArgumentException("Invalid Input");

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "insert into tenants(unit_id, name, email, phone) values(?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ps.setString(2, name);
			ps.setString(3, email);
			ps.setString(4, phone);
			ps.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new IllegalArgumentException();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Tenant> getTenant(int unitID) {
		List<Tenant> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select t.unit_id, t.name, p.address, u.apt_number, t.email, t.phone " +
					"from tenants t " +
					"left join units u on t.unit_id = u.unit_id " +
					"left join properties p on u.property_id = p.property_id " +
					"where u.unit_id = ? " +
					"group by t.unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Tenant(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static List<Tenant> getTenants() {
		List<Tenant> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select t.unit_id, t.name, p.address, u.apt_number, t.email, t.phone " +
					"from tenants t " +
					"left join units u on t.unit_id = u.unit_id " +
					"left join properties p on u.property_id = p.property_id " +
					"group by t.unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Tenant(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeTenant(Tenant toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from tenants where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, toRemove.getUnitID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
