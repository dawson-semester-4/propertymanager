package propertymanager.backend;

public class House extends Property {
	public House(int propertyID, String address, String price) {
		super(propertyID, address, "House", "1", price);
	}
}
