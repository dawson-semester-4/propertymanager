package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Lease {

	private final int unitID;
	private final String name;
	private final String address;
	private final String aptNum;
	private final String startDate;
	private final String endDate;
	private final String monthlyRent;

	public Lease(int unitID, String name, String address, String unit, String startDate, String endDate, String monthlyRent) {
		this.unitID = unitID;
		this.name = name;
		this.address = address;
		this.aptNum = unit;
		this.startDate = startDate;
		this.endDate = endDate;
		this.monthlyRent = monthlyRent;
	}

	public int getUnitID() {
		return unitID;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getAptNum() {
		return aptNum;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public String getMonthlyRent() {
		return monthlyRent;
	}

	public static void update(String field, int leaseDiscovery, String newValue) {
		switch (field) {
			case "Start Date" -> field = "start_date";
			case "End Date" -> field = "end_date";
			case "Monthly Rent" -> field = "rent";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update leases set " + field + " = ? where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, leaseDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addLease(int unitID, Date startDate, Date endDate, float rent) throws IllegalArgumentException {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "insert into leases(unit_id, start_date, end_date, rent) values(?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ps.setDate(2, startDate);
			ps.setDate(3, endDate);
			ps.setFloat(4, rent);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Lease> getLease(int unitID) {
		List<Lease> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select l.unit_id, t.name, p.address, u.apt_number, l.start_date, l.end_date, l.rent " +
					"from leases l " +
					"left join tenants t on l.unit_id = t.unit_id " +
					"left join units u on t.unit_id = u.unit_id " +
					"left join properties p on u.property_id = p.property_id " +
					"where u.unit_id = ? " +
					"group by l.unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Lease(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static List<Lease> getLeases() {
		List<Lease> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select l.unit_id, t.name, p.address, u.apt_number, l.start_date, l.end_date, l.rent " +
					"from leases l " +
					"left join tenants t on l.unit_id = t.unit_id " +
					"left join units u on t.unit_id = u.unit_id " +
					"left join properties p on u.property_id = p.property_id " +
					"group by l.unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Lease(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeLease(Lease toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from leases where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, toRemove.getUnitID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
