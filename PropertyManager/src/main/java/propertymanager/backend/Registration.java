package propertymanager.backend;

import java.sql.*;

public class Registration extends Security {
	public static void register(String name, String address, int aptNum, String password) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			boolean adminAccess = address.equalsIgnoreCase("admin");
			String sql = "insert into login(unit_id, admin_access, name, password_hash, salt) values(?, ?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			// if there is admin access, aptNum should not exist
			if (adminAccess || aptNum == -1)
				ps.setNull(1, Types.INTEGER);
			else
				ps.setInt(1, getUnitID(aptNum));
			ps.setBoolean(2, adminAccess);
			ps.setString(3, name);
			byte[] salt = generateSalt();
			String hashedPassword = hashPassword(password, salt);
			ps.setString(4, hashedPassword);
			ps.setString(5, new String(salt));
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
