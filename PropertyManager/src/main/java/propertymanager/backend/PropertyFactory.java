package propertymanager.backend;

public class PropertyFactory {

	public static Property getProperty(int propertyID, String address, String type, String unitCount, String price, String condoFee) {
		switch (type) {
			case "Plex" -> {
				return new Plex(propertyID, address, unitCount, price);
			}
			case "House" -> {
				return new House(propertyID, address, price);
			}
			case "Condo" -> {
				return new Condo(propertyID, address, price, condoFee);
			}
		}
		return null;
	}
}
