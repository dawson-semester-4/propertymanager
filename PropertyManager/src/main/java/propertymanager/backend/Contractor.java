package propertymanager.backend;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Contractor {
	private final int contractorID;
	private final int propertyID;
	private final String propertyAddress;
	private final String name;
	private final String type;
	private final String email;
	private final String phone;

	public Contractor(int contractorID, int propertyID, String propertyAddress, String name, String type, String email, String phone) {
		this.contractorID = contractorID;
		this.propertyID = propertyID;
		this.propertyAddress = propertyAddress;
		this.name = name;
		this.type = type;
		this.email = email;
		this.phone = phone;
	}

	public int getContractorIDint() {
		return contractorID;
	}

	public String getContractorID() {
		return String.valueOf(contractorID);
	}

	public int getPropertyID() {
		return propertyID;
	}

	public String getPropertyAddress() {
		return propertyAddress;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public static void update(String field, int contractorDiscovery, String newValue) {
		switch (field) {
			case "ID" -> field = "contractor_id";
			case "Name" -> field = "name";
			case "Type" -> field = "type";
			case "Email" -> field = "email";
			case "Phone #" -> field = "phone";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update maintenance_contractors set " + field + " = ? where property_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, contractorDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addContractor(String propertyAddress, String name, String type, String email, String phone) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select property_id from properties where address like ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, propertyAddress);
			ResultSet rs = ps.executeQuery();
			int propertyID = -1;
			if (rs.next()) {
				propertyID = rs.getInt(1);
			}

			String sql2 = "insert into maintenance_contractors(property_id, name, type, email, phone) values (?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(sql2);
			ps.setInt(1, propertyID);
			ps.setString(2, name);
			ps.setString(3, type);
			ps.setString(4, email);
			ps.setString(5, phone);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Contractor> getContractors() {
		// Creating 2d array to store query
		List<Contractor> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select c.contractor_id, p.property_id, p.address, c.name, c.type, c.email, c.phone " +
					"from maintenance_contractors c " +
					"left join properties p on c.property_id = p.property_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Contractor(
						rs.getInt(1),
						rs.getInt(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeContractor(Contractor toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from maintenance_contractors where contractor_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, toRemove.getContractorID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
