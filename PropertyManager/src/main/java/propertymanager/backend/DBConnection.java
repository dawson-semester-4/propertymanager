package propertymanager.backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//singleton pattern
public class DBConnection {

	private static DBConnection connInstance = null;
	private static Connection conn = null;

	private DBConnection() {
	}

	public static DBConnection getInstance() {
		if (connInstance == null)
			connInstance = new DBConnection();
		return connInstance;
	}

	public Connection getConnection() throws SQLException {
		if (conn == null) {
			String url = "jdbc:mysql://localhost:3306/propertymanager";
			String username = "root";
			String password = "Python";
			conn = DriverManager.getConnection(url, username, password);
			if (!tablesExist()) {
				manageTables(true); // creates tables
				insertData(); // inserts data into tables
			}
		}
		conn.setAutoCommit(false);
		return conn;
	}

	private static boolean tablesExist() throws SQLException {
		ResultSet tables = conn.getMetaData().getTables(null, null, null, new String[]{"TABLE"});
		List<String> tableNames = new ArrayList<>();
		Collections.addAll(tableNames, "login", "tenants", "payment", "leases", "units", "mortgage", "properties",
		                   "maintenance_log", "maintenance_contractors", "financial_institutions");

		List<String> actualTableNames = new ArrayList<>();

		while (tables.next()) {
			if (tables.getString("TABLE_CAT").equalsIgnoreCase("propertymanager")) {
				actualTableNames.add(tables.getString("TABLE_NAME"));
			}
		}

		if (tableNames.size() != actualTableNames.size()) {
			System.out.println("Not all tables exist!");
			return false;
		} else {
			// check if table names match each other
			for (String tableName : tableNames) {
				if (!actualTableNames.contains(tableName)) {
					return false;
				}
			}
		}
		return true;
	}

	private static String[] getStatementsFromFile(String path) throws IOException {
		StringBuilder sb = new StringBuilder();
		FileReader fr = new FileReader(path);
		BufferedReader br = new BufferedReader(fr);
		String s;
		while ((s = br.readLine()) != null) {
			if (s.startsWith("--"))
				continue;
			sb.append("\n");
			sb.append(s);
		}
		br.close();

		return sb.toString().split(";");
	}

	private static void updateDB(String [] statements) {
		for (String statement : statements) {
			if (!statement.isBlank()) {
				try {
					PreparedStatement ps = conn.prepareStatement(statement);
					ps.executeUpdate();
				} catch (SQLException e) {
					System.out.println(e.getMessage());
				}
			}
		}
	}

	private static boolean tablesDropped = false;

	public static void manageTables(boolean createTables) {
		try {
			// drop tables, then add them again
			if (!tablesDropped) {
				System.out.println("Dropping tables then creating...");
				tablesDropped = true; // this must go first, or stack overflow
				manageTables(false);
			}

			String path = createTables ? "M&N_Property_Manager_create.sql" : "M&N_Property_Manager_drop.sql";

			String[] statements = getStatementsFromFile(path);
			updateDB(statements);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void insertData() {
		try {
			String path = "M&N_Property_Manager_insert.sql";
			String[] statements = getStatementsFromFile(path);
			updateDB(statements);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void commit() {
		try {
			DBConnection.getInstance().getConnection().commit();
//			System.out.println("COMMITTED DB");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void rollback() {
		try {
			DBConnection.getInstance().getConnection().rollback();
			System.out.println("ROLLED BACK DB");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
