package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Mortgage {
	private final int propertyID;
	private final String address;
	private final String instName;
	private final String price;
	private final String balance;
	private final String monthlyPayment;
	private final String interestRate;

	public Mortgage(int propertyID, String address, String instName, String price, String balance, String monthlyPayment, String interestRate) {
		this.propertyID = propertyID;
		this.address = address;
		this.instName = instName;
		this.price = price;
		this.balance = balance;
		this.monthlyPayment = monthlyPayment;
		this.interestRate = interestRate;
	}

	public int getPropertyID() {
		return propertyID;
	}

	public String getAddress() {
		return address;
	}

	public String getInstName() {
		return instName;
	}

	public String getPrice() {
		return price;
	}

	public String getBalance() {
		return balance;
	}

	public String getMonthlyPayment() {
		return monthlyPayment;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public static void update(String field, int unitDiscovery, String newValue) {
		switch (field) {
			case "Price" -> field = "price";
			case "Balance" -> field = "balance";
			case "Monthly Payment" -> field = "monthly_payment";
			case "Interest Rate" -> field = "interest";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update mortgage set " + field + " = ? where property_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, unitDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addMortgage(int propertyID, String instName, float price, float balance, float monthlyPayment, float interestRate) throws IllegalArgumentException {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select fin_institution_id from financial_institutions where name = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, instName);
			ResultSet rs = ps.executeQuery();
			int institutionID = -1;
			if (rs.next())
				institutionID = rs.getInt(1);

			if (institutionID == -1)
				throw new IllegalArgumentException("Invalid inst. Name");

			String sql2 = "insert into mortgage(property_id, fin_institution_id, price, balance, monthly_payment, interest) values(?, ?, ?, ?, ?, ?)";
			ps = conn.prepareStatement(sql2);
			ps.setInt(1, propertyID);
			ps.setInt(2, institutionID);
			ps.setFloat(3, price);
			ps.setFloat(4, balance);
			ps.setFloat(5, monthlyPayment);
			ps.setFloat(6, interestRate);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Mortgage> getMortgage(int propertyID) {
		List<Mortgage> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select p.property_id, p.address, fi.name, m.price, m.balance, m.monthly_payment, m.interest " +
					"from mortgage m " +
					"left join properties p on m.property_id = p.property_id " +
					"left join financial_institutions fi on m.fin_institution_id = fi.fin_institution_id " +
					"where m.property_id = ? " +
					"group by m.property_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, propertyID);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Mortgage(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static List<Mortgage> getMortgages() {
		List<Mortgage> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select p.property_id, p.address, fi.name, m.price, m.balance, m.monthly_payment, m.interest " +
					"from mortgage m " +
					"left join properties p on m.property_id = p.property_id " +
					"left join financial_institutions fi on m.fin_institution_id = fi.fin_institution_id " +
					"group by m.property_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new Mortgage(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6),
						rs.getString(7)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeMortgage(Mortgage toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from mortgage where property_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, toRemove.getPropertyID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
