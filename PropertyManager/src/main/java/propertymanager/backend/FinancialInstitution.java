package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class FinancialInstitution {

	private final int institutionID;
	private final String institutionName;
	private final String interestRate;

	public FinancialInstitution(int fin_institution_id, String institutionName, String interestRate) {
		this.institutionID = fin_institution_id;
		this.institutionName = institutionName;
		this.interestRate = interestRate;
	}

	public int getInstitutionID() {
		return institutionID;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public String getInterestRate() {
		return interestRate;
	}

	public static void update(String field, int unitDiscovery, String newValue) {
		switch (field) {
			case "Institution Name" -> field = "name";
			case "Interest Rate" -> field = "interest";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "update financial_institutions set " + field + " = ? where fin_institution_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, unitDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addInstitution(String institutionName, float interestRate) throws IllegalArgumentException {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "insert into financial_institutions(name, interest) values(?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, institutionName);
			ps.setFloat(2, interestRate);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<String> getFinancialInstitutionNames() {
		List<String> institutionNames = new ArrayList<>();
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select name from financial_institutions";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				institutionNames.add(rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return institutionNames;
	}

	public static List<FinancialInstitution> getFinancialInstitutions() {
		List<FinancialInstitution> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select fin_institution_id, name, interest from financial_institutions";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				rsTable.add(new FinancialInstitution(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static void removeFinancialInstitution(FinancialInstitution toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from financial_institutions where fin_institution_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, toRemove.getInstitutionID());
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
