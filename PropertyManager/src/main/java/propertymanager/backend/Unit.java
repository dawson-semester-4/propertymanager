package propertymanager.backend;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Unit {
	private final int unitID;
	private final String aptNumber;
	private final String tenantName;
	private final String maintenanceIncidents;
	private final String monthlyRent;
	private final String condoFee;

	public Unit(int unitID, String aptNumber, String tenantName, String maintenanceIncidents, String monthlyRent, String condoFee) {
		this.unitID = unitID;
		this.aptNumber = aptNumber;
		this.tenantName = tenantName;
		this.maintenanceIncidents = maintenanceIncidents;
		this.monthlyRent = monthlyRent;
		this.condoFee = condoFee;
	}

	public int getUnitID() {
		return unitID;
	}

	public String getTenantName() {
		return tenantName;
	}

	public String getMaintenanceIncidents() {
		return maintenanceIncidents;
	}

	public String getAptNumber() {
		return aptNumber;
	}

	public String getMonthlyRent() {
		return monthlyRent;
	}

	public String getCondoFee() {
		return condoFee;
	}

	public static int getPropertyIDFromAddress(String addressSelected) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select property_id from properties where address = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, addressSelected);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static boolean isUnitsFilled(int unitID, int unitsExisting) {
		try {
			// get units that should be in the property provided
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select p.unit_count from properties p join units u on p.property_id = u.property_id where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, unitID);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt(1) == unitsExisting)
					return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void update(String field, int unitDiscovery, String newValue) {
		switch (field) {
			case "Apt #" -> field = "apt_number";
			case "Monthly Rent" -> field = "monthly_rent";
			case "Condo Fee" -> field = "condo_fee";
		}
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			conn.setAutoCommit(false);
			String sql = "update units set " + field + " = ? where unit_id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, newValue);
			ps.setInt(2, unitDiscovery);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void addUnit(int propertyID, int aptNumber, float monthlyRent, float condoFee) throws IllegalArgumentException {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "insert into units(property_id, apt_number, monthly_rent, condo_fee) values(?, ?, ?, ?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, propertyID);
			ps.setInt(2, aptNumber);
			ps.setFloat(3, monthlyRent);
			if (condoFee == -1)
				ps.setNull(4, Types.FLOAT);
			else
				ps.setFloat(4, condoFee);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void removeUnit(String address, Unit toRemove) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "delete from units where apt_number = ? and unit_id = (" +
					"select unit_id from (select unit_id from units u join properties p on u.property_id = p.property_id where p.address = ?) as u)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, toRemove.getAptNumber());
			ps.setString(2, address);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static List<Unit> getUnits(String address) {
		// Creating 2d array to store query
		List<Unit> rsTable = new ArrayList<>();

		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select u.unit_id, u.apt_number, coalesce(t.name, 'n/a'), count(m.unit_id) as 'incidents', u.monthly_rent, coalesce(u.condo_fee, 'n/a')" +
					"from units u " +
					"left join tenants t on u.unit_id = t.unit_id " +
					"left join maintenance_log m on u.unit_id = m.unit_id " +
					"where property_id = (select property_id from properties where address like ? limit 1) " +
					"group by unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, address + "%");
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				if (rs.getString(1) == null)
					return rsTable;

				rsTable.add(new Unit(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rsTable;
	}

	public static Unit getUnit(String address, String apt) {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			String sql = "select u.unit_id, u.apt_number, coalesce(t.name, 'n/a'), count(m.unit_id) as 'incidents', u.monthly_rent, coalesce(u.condo_fee, 'n/a')" +
					"from units u " +
					"left join tenants t on u.unit_id = t.unit_id " +
					"left join maintenance_log m on u.unit_id = m.unit_id " +
					"where property_id = (select property_id from properties where address like ? limit 1) " +
					"and apt_number like ?" +
					"group by unit_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, address + "%");
			ps.setString(2, apt + "%");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return new Unit(
						rs.getInt(1),
						rs.getString(2),
						rs.getString(3),
						rs.getString(4),
						rs.getString(5),
						rs.getString(6)
				);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
