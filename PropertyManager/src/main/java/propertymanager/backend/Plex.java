package propertymanager.backend;

public class Plex extends Property {
	public Plex(int propertyID, String address, String unitCount, String price) {
		super(propertyID, address, "Plex", unitCount, price);
	}
}
