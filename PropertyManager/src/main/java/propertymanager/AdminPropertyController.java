package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.DBConnection;
import propertymanager.backend.Property;
import propertymanager.backend.PropertyFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class AdminPropertyController extends Controller {
	@FXML
	private CheckBox editable;
	@FXML
	private TableView<Property> table;
	@FXML
	private TableColumn<Property, String> address;
	@FXML
	private TableColumn<Property, String> type;
	@FXML
	private TableColumn<Property, String> unitCount;
	@FXML
	private TableColumn<Property, String> price;

	@FXML
	private VBox actionButtons;
	@FXML
	private Button refresh;
	@FXML
	private Button commit;

	@FXML
	private HBox addDetails;
	@FXML
	private TextField addressInput;
	@FXML
	private ComboBox<String> typeInput;
	@FXML
	private TextField unitInput;

	private static Property propertySelected;
	private static boolean switchBackToProperties = false;
	private static int unitsToAdd = 1;

	public static Property getPropertySelected() {
		return propertySelected;
	}

	public static boolean getSwitchBackToProperties() {
		return switchBackToProperties;
	}

	public static int getUnitsToAdd() {
		return unitsToAdd;
	}

	@FXML
	private void initialize() {
		propertySelected = null;

		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		initColumn(address);
		type.setCellValueFactory(new PropertyValueFactory<>("type"));
		initColumn(type);
		unitCount.setCellValueFactory(new PropertyValueFactory<>("unitCount"));
		initColumn(unitCount);
		price.setCellValueFactory(new PropertyValueFactory<>("price"));
		initColumn(price);

		table.getItems().addAll(Property.getProperties());

		typeInput.getItems().addAll("Plex", "House", "Condo");
		typeInput.getSelectionModel().selectFirst();

		setEditable();


		if (switchBackToProperties) {
			addMortgage();
			switchBackToProperties = false;
		}
	}

	private void initColumn(TableColumn<Property, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			String recordIdentifier = event.getRowValue().getAddress();
			Property.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	@FXML
	private void setEditable() {
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			conn.setAutoCommit(editable.isSelected());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
		setUnitCount();
	}

	@FXML
	private void goToUnits() {
		propertySelected = table.getSelectionModel().getSelectedItem();
		App.setScene("admin_units");
	}

	private void goToUnits(Property p) {
		propertySelected = p;
		App.setScene("admin_units");
	}

	@FXML
	private void deleteSelectedRow() {
		Property.removeProperty(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	@FXML
	private void setUnitCount() {
		if (!typeInput.getValue().equals("Plex")) {
			unitInput.setText("1");
			unitInput.setDisable(true);
		} else {
			unitInput.setDisable(false);
		}
	}

	void refresh() {
		table.getItems().clear();
		table.getItems().addAll(Property.getProperties());
	}

	@FXML
	private void addProperty() {
		try {
			String addressInputText = addressInput.getText().trim();
			Property property = PropertyFactory.getProperty(-1, addressInputText, typeInput.getValue(), unitInput.getText().trim(), null, null);
			assert property != null;
			int propertyUnitCountNum;
			try {
				propertyUnitCountNum = Integer.parseInt(property.getUnitCount());
			} catch (NumberFormatException e) {
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText("Invalid Input");
				alert.setContentText("Units and/or price was entered incorrectly!");
				alert.showAndWait();
				return;
			}
			Property.addProperty(property.getAddress(), property.getType(), propertyUnitCountNum);
			refresh();

			// BEFORE MORTGAGE, SWITCH TO UNITS TO ADD INFO THEN COME BACK HERE AFTER
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Add Units");
			alert.setHeaderText("Property Added");
			alert.setContentText("Please add the information for the unit(s) of this property");
			alert.showAndWait();
			switchBackToProperties = true; // tells AdminUnitsController to switch back to property view after addition of units
			unitsToAdd = propertyUnitCountNum; // sets number of units needed to exit out of unit adding\
			goToUnits(property);
		} catch (IllegalArgumentException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(e.getMessage());
			alert.showAndWait();
		}
	}

	private void addMortgage() {
		// ask if there is a mortgage
		ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
		ButtonType no = new ButtonType("No", ButtonBar.ButtonData.NO);
		Alert alert2 = new Alert(Alert.AlertType.CONFIRMATION, "Add associated mortgage to this property?", yes, no);
		alert2.setTitle("Add Mortgage");
		alert2.setHeaderText("Property Saved.");
		alert2.showAndWait().ifPresent(btn -> {
			if (btn == yes) {
				App.setScene("admin_mortgage");
			}
		});
	}
}
