package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import propertymanager.backend.Lease;
import propertymanager.backend.TenantLease;

public class TenantLeaseController extends Controller {
	@FXML
	private TableView<Lease> table;
	@FXML
	private TableColumn<Lease, String> name;
	@FXML
	private TableColumn<Lease, String> address;
	@FXML
	private TableColumn<Lease, String> aptNum;
	@FXML
	private TableColumn<Lease, String> startDate;
	@FXML
	private TableColumn<Lease, String> endDate;
	@FXML
	private TableColumn<Lease, String> monthlyRent;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		name.setCellValueFactory(new PropertyValueFactory<>("name"));
		name.setCellFactory(TextFieldTableCell.forTableColumn());
		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		address.setCellFactory(TextFieldTableCell.forTableColumn());
		aptNum.setCellValueFactory(new PropertyValueFactory<>("aptNum"));
		aptNum.setCellFactory(TextFieldTableCell.forTableColumn());
		startDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
		startDate.setCellFactory(TextFieldTableCell.forTableColumn());
		endDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));
		endDate.setCellFactory(TextFieldTableCell.forTableColumn());
		monthlyRent.setCellValueFactory(new PropertyValueFactory<>("monthlyRent"));
		monthlyRent.setCellFactory(TextFieldTableCell.forTableColumn());

		String tenantName = LoginController.getNameGiven();
		int tenantUnit = LoginController.getUnitGiven();

		try {
			table.getItems().addAll(TenantLease.getTenantLease(tenantName, tenantUnit));
		} catch (IllegalArgumentException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Lease information does not exist");
			alert.setContentText(e.getMessage());
			alert.showAndWait();
		}
	}

	void refresh() {
	}
}
