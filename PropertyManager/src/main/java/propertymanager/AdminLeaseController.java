package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.*;

import java.sql.Date;

public class AdminLeaseController extends Controller {

	@FXML
	private CheckBox editable;
	@FXML
	private TableView<Lease> table;
	@FXML
	private TableColumn<Lease, String> name;
	@FXML
	private TableColumn<Lease, String> address;
	@FXML
	private TableColumn<Lease, String> apt;
	@FXML
	private TableColumn<Lease, String> startDate;
	@FXML
	private TableColumn<Lease, String> endDate;
	@FXML
	private TableColumn<Lease, String> monthlyRent;

	@FXML
	private HBox addDetails;

	@FXML
	private VBox actionButtons;

	@FXML
	private Label inputInfo;

	@FXML
	private VBox viewButtons;

	@FXML
	private TextField propertyAddressInput;

	@FXML
	private TextField unitInput;

	@FXML
	private DatePicker startDateInput;

	@FXML
	private DatePicker endDateInput;

	@FXML
	private TextField monthlyRentInput;

	private static Property propertyUsed;
	private static Unit unitUsed;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		if (AdminUnitsController.getSwitchBackToUnits()) {
			viewButtons.setDisable(true);
			editable.setSelected(true);
			setEditable();
		}

		name.setCellValueFactory(new PropertyValueFactory<>("name"));
		initColumn(name);
		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		initColumn(address);
		apt.setCellValueFactory(new PropertyValueFactory<>("aptNum"));
		initColumn(apt);
		startDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
		initColumn(startDate);
		endDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));
		initColumn(endDate);
		monthlyRent.setCellValueFactory(new PropertyValueFactory<>("monthlyRent"));
		initColumn(monthlyRent);

		// functionality for buttons on units view
//        unit = Unit.
		propertyAddressInput.textProperty().addListener((obs, oldText, newText) -> {
			propertyUsed = Property.getProperty(newText);
			unitInput.setDisable(!(propertyUsed instanceof Plex));

			// put only numbers from the address into the apt number input
			String address = propertyAddressInput.getText().trim();
			unitInput.setText(onlyDigits(address));
			searchUnit();
			refresh();
		});
		unitInput.textProperty().addListener((obs, oldText, newText) -> {
			unitUsed = Unit.getUnit(propertyAddressInput.getText().trim(), unitInput.getText().trim());
			searchUnit();
			refresh();
		});
	}

	private void initColumn(TableColumn<Lease, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getUnitID();
			Lease.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	private void searchUnit() {
		unitUsed = Unit.getUnit(propertyAddressInput.getText().trim(), unitInput.getText().trim());
		inputInfo.setVisible(unitUsed != null);
	}

	@FXML
	private void addLease() {
		float monthlyRentInputNum;
		try {
			monthlyRentInputNum = Float.parseFloat(monthlyRentInput.getText().trim());
		} catch (NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Input");
			alert.setContentText("Monthly rent entered incorrectly!");
			alert.showAndWait();
			return;
		}
		try {
			Lease.addLease(unitUsed.getUnitID(), Date.valueOf(startDateInput.getValue()), Date.valueOf(endDateInput.getValue()), monthlyRentInputNum);
		} catch (NullPointerException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Input");
			alert.setContentText("All fields are required!");
			alert.showAndWait();
			return;
		} catch (IllegalArgumentException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("One lease allowed");
			alert.setContentText("You can't enter more than one lease!");
			alert.showAndWait();
			return;
		}
		refresh();
	}

	@FXML
	private void deleteSelectedRow() {
		Lease.removeLease(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		if (unitUsed == null)
			table.getItems().addAll(Lease.getLeases());
		else
			table.getItems().addAll(Lease.getLease(unitUsed.getUnitID()));
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}
