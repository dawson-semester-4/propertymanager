package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import propertymanager.backend.TenantPayment;

public class TenantPaymentController extends Controller {
	@FXML
	private TableView<TenantPayment> table;
	@FXML
	private TableColumn<TenantPayment, String> address;
	@FXML
	private TableColumn<TenantPayment, String> unit;
	@FXML
	private TableColumn<TenantPayment, String> paymentDueDate;
	@FXML
	private TableColumn<TenantPayment, String> datePaid;
	@FXML
	private TableColumn<TenantPayment, String> paymentMethod;
	@FXML
	private TableColumn<TenantPayment, String> amount;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		address.setCellFactory(TextFieldTableCell.forTableColumn());
		unit.setCellValueFactory(new PropertyValueFactory<>("aptNum"));
		unit.setCellFactory(TextFieldTableCell.forTableColumn());
		paymentDueDate.setCellValueFactory(new PropertyValueFactory<>("paymentDueDate"));
		paymentDueDate.setCellFactory(TextFieldTableCell.forTableColumn());
		datePaid.setCellValueFactory(new PropertyValueFactory<>("datePaid"));
		datePaid.setCellFactory(TextFieldTableCell.forTableColumn());
		paymentMethod.setCellValueFactory(new PropertyValueFactory<>("paymentMethod"));
		paymentMethod.setCellFactory(TextFieldTableCell.forTableColumn());
		amount.setCellValueFactory(new PropertyValueFactory<>("amount"));
		amount.setCellFactory(TextFieldTableCell.forTableColumn());

		String tenantName = LoginController.getNameGiven();
		int tenantUnit = LoginController.getUnitGiven();

		try {
			table.getItems().addAll(TenantPayment.getTenantPayments(tenantName, tenantUnit));
		} catch (IllegalArgumentException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Payment information does not exist");
			alert.setContentText(e.getMessage());
			alert.showAndWait();
		}
	}

	void refresh() {
	}
}
