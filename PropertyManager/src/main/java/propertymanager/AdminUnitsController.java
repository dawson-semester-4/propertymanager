package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.*;

import java.util.List;

public class AdminUnitsController extends Controller {
	@FXML
	private TableView<Unit> table;
	@FXML
	private TableColumn<Unit, String> aptNumber;
	@FXML
	private TableColumn<Unit, String> tenantName;
	@FXML
	private TableColumn<Unit, String> maintenanceIncidents;
	@FXML
	private TableColumn<Unit, String> monthlyRent;
	@FXML
	private TableColumn<Unit, String> condoFee;

	@FXML
	private HBox addDetails;

	@FXML
	private VBox actionButtons;

	@FXML
	private TextField propertyAddressInput;

	@FXML
	private Label propertyType;

	@FXML
	private TextField aptNumberInput;

	@FXML
	private TextField monthlyRentInput;

	@FXML
	private TextField condoFeeInput;

	@FXML
	private CheckBox editable;


	@FXML
	private VBox viewButtons;

	private static Property propertyUsed;
	private static Unit unitSelected;
	private static boolean leaseAdded = false;
	private static boolean tenantAdded = false;
	private static boolean switchBackToUnits = false;

	public static Property getPropertyUsed() {
		return propertyUsed;
	}

	public static Unit getUnitSelected() {
		return unitSelected;
	}

	public static boolean isLeaseAdded() {
		return leaseAdded;
	}

	public static void setLeaseAdded(boolean leaseAdded) {
		AdminUnitsController.leaseAdded = leaseAdded;
	}

	public static boolean isTenantAdded() {
		return tenantAdded;
	}

	public static void setTenantAdded(boolean tenantAdded) {
		AdminUnitsController.tenantAdded = tenantAdded;
	}

	public static boolean getSwitchBackToUnits() {
		return switchBackToUnits;
	}

	public static void setSwitchBackToUnits(boolean switchBackToUnits) {
		AdminUnitsController.switchBackToUnits = switchBackToUnits;
	}

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		setUserNameLabel();

		if (AdminPropertyController.getSwitchBackToProperties()) {
			viewButtons.setDisable(true);
			editable.setSelected(true);
			setEditable();
		}

		aptNumber.setCellValueFactory(new PropertyValueFactory<>("aptNumber"));
		initColumn(aptNumber);
		tenantName.setCellValueFactory(new PropertyValueFactory<>("tenantName"));
		initColumn(tenantName);
		maintenanceIncidents.setCellValueFactory(new PropertyValueFactory<>("maintenanceIncidents"));
		initColumn(maintenanceIncidents);
		monthlyRent.setCellValueFactory(new PropertyValueFactory<>("monthlyRent"));
		initColumn(monthlyRent);
		condoFee.setCellValueFactory(new PropertyValueFactory<>("condoFee"));
		initColumn(condoFee);

		propertyAddressInput.textProperty().addListener((obs, oldText, newText) -> {
			if (newText.length() >= 3) {
				propertyUsed = Property.getProperty(newText);
				searchUnit();
				disableUnitInputs();
			} else {
				table.getItems().clear();
			}
		});

		propertyUsed = AdminPropertyController.getPropertySelected();
		if (propertyUsed != null) {
			propertyAddressInput.setText(propertyUsed.getAddress());
			propertyType.setText(propertyType.getText() + propertyUsed.getType());
		}

		if (getSwitchBackToUnits())
			switchBackToUnits = false;
	}

	private void disableUnitInputs() {
		if (propertyUsed instanceof Condo) {
			condoFeeInput.setDisable(false);
			aptNumberInput.setDisable(true);
		} else if (propertyUsed instanceof Plex) {
			aptNumberInput.setDisable(false);
		} else {
			condoFeeInput.setDisable(true);
			aptNumberInput.setDisable(true);
		}
	}

	private void initColumn(TableColumn<Unit, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getUnitID();
			Unit.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	@FXML
	private void goToTenant() {
		unitSelected = table.getSelectionModel().getSelectedItem();
		App.setScene("admin_tenants");
	}

	@FXML
	private void goToMaintenance() {
		App.setScene("admin_maintenanceLog");
	}

	@FXML
	private void addUnit() {
		if (propertyAddressInput.getText().trim().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("No Address");
			alert.setHeaderText("No Address to add a unit to!");
			alert.setContentText("Input an address to add a unit!");
			alert.showAndWait();
			return;
		}

		//check if property has enough space to add a unit
		List<Unit> tableItems = table.getItems();
		if (tableItems.size() > 0 && Unit.isUnitsFilled(tableItems.get(0).getUnitID(), tableItems.size())) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Units Filled");
			alert.setHeaderText("Units Filled!");
			alert.setContentText(tableItems.size() + " units filled. To add more, adjust the unit count of the property." +
					"switching back to properties.");
			alert.showAndWait();
			App.setScene("admin_property");
			return;
		}

		int aptNumber;
		float monthlyRent;
		float condoFee;
		try {
			aptNumber = Integer.parseInt(aptNumberInput.getText().trim());
			monthlyRent = Float.parseFloat(monthlyRentInput.getText().trim());
			if (propertyUsed instanceof Condo)
				condoFee = Float.parseFloat(condoFeeInput.getText().trim());
			else
				condoFee = -1;
		} catch (NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Input");
			alert.setContentText("Number(s) entered incorrectly!");
			alert.showAndWait();
			return;
		}
		try {
			Unit.addUnit(Unit.getPropertyIDFromAddress(propertyAddressInput.getText().trim()), aptNumber, monthlyRent, condoFee);
			refresh();

			// Add tenant to unit?
			ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
			ButtonType no = new ButtonType("No", ButtonBar.ButtonData.NO);
			Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Add a tenant to this unit?", yes, no);
			alert.setTitle("Tenant");
			alert.setHeaderText("Unit added");
			alert.showAndWait().ifPresent(btn -> {
				if (btn == yes) {
					App.setScene("admin_tenants");
					setSwitchBackToUnits(true);
				} else {
					// Add lease to unit?
					alert.setContentText("Add a lease to this unit?");
					alert.showAndWait().ifPresent(btn2 -> {
						if (btn2 == yes) {
							App.setScene("admin_lease");
							setSwitchBackToUnits(true);
						}
					});
				}
			});
		} catch (IllegalArgumentException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(e.getMessage());
			alert.showAndWait();
		}


		if (AdminPropertyController.getSwitchBackToProperties()) {
			// Add another unit(s) to the property?
			int unitsToAdd = AdminPropertyController.getUnitsToAdd() - tableItems.size();
			if (unitsToAdd > 0) {
				ButtonType yes2 = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
				ButtonType later = new ButtonType("Later", ButtonBar.ButtonData.NO);
				Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Add another unit to this property? You have " +
						unitsToAdd + " more units to add.", yes2, later);
				alert.setTitle("Units");
				alert.setHeaderText("Unit added");
				alert.showAndWait().ifPresent(btn -> {
					if (btn == later) {
						App.setScene("admin_property");
					}
				});
			} else {
				Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "No more units to add. Switching back to properties.");
				alert2.setTitle("Units");
				alert2.setHeaderText("Unit added");
				alert2.showAndWait();
				App.setScene("admin_property");
			}
		}
	}

	private void searchUnit() {
		table.getItems().clear();
		aptNumberInput.clear();
		if (propertyUsed != null) {
			String address = propertyUsed.getAddress();
			table.getItems().addAll(Unit.getUnits(address));

			// put only numbers from the address into the apt number input
			String noDigits = address.replaceAll("^\\d+", "");
			String onlyDigits = address.replaceAll(noDigits, "");
			aptNumberInput.setText(onlyDigits);
		}
	}

	@FXML
	private void deleteSelectedRow() {
		Unit.removeUnit(propertyUsed.getAddress(), table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		table.getItems().addAll(Unit.getUnits(propertyAddressInput.getText().trim()));
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}