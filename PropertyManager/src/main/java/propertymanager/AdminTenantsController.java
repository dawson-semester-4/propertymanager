package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.*;

import java.util.regex.Pattern;

public class AdminTenantsController extends Controller {
	@FXML
	private CheckBox editable;
	@FXML
	private TableView<Tenant> table;
	@FXML
	private TableColumn<Tenant, String> name;
	@FXML
	private TableColumn<Tenant, String> address;
	@FXML
	private TableColumn<Tenant, String> apt;
	@FXML
	private TableColumn<Tenant, String> email;
	@FXML
	private TableColumn<Tenant, String> phone;

	@FXML
	private HBox addDetails;

	@FXML
	private VBox actionButtons;

	@FXML
	private Label inputInfo;

	@FXML
	private TextField propertyAddressInput;

	@FXML
	private TextField unitInput;

	@FXML
	private TextField nameInput;

	@FXML
	private TextField emailInput;

	@FXML
	private TextField phoneInput;

	private static Property propertyUsed;
	private static Unit unitUsed;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		name.setCellValueFactory(new PropertyValueFactory<>("name"));
		initColumn(name);
		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		initColumn(address);
		apt.setCellValueFactory(new PropertyValueFactory<>("aptNum"));
		initColumn(apt);
		email.setCellValueFactory(new PropertyValueFactory<>("email"));
		initColumn(email);
		phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
		initColumn(phone);

		propertyAddressInput.textProperty().addListener((obs, oldText, newText) -> {
			propertyUsed = Property.getProperty(newText);
			unitInput.setDisable(!(propertyUsed instanceof Plex));

			// put only numbers from the address into the apt number input
			String address = propertyAddressInput.getText().trim();
			unitInput.setText(onlyDigits(address));
			searchUnit();
			refresh();
		});

		unitInput.textProperty().addListener((obs, oldText, newText) -> {
			searchUnit();
			refresh();
		});

		if (AdminUnitsController.getSwitchBackToUnits()) {
			propertyUsed = AdminUnitsController.getPropertyUsed();
			propertyAddressInput.setText(propertyUsed.getAddress());
			setEditable();
		}

		refresh();
	}

	private void initColumn(TableColumn<Tenant, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getUnitID();
			Tenant.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	private void searchUnit() {
		unitUsed = Unit.getUnit(propertyAddressInput.getText().trim(), unitInput.getText().trim());
		inputInfo.setVisible(unitUsed != null);
	}

	@FXML
	private void addTenant() {
		if (unitUsed == null) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Select a unit");
			alert.setContentText("Select a unit to add a tenant to with an address");
			alert.showAndWait();
			return;
		}

		if (!verifyEmail(emailInput.getText())) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Email Input");
			alert.setContentText("Email entered incorrectly!");
			alert.showAndWait();
			return;
		}

		String phoneNum = phoneNumOnlyDigits(phoneInput.getText().trim());
		if (phoneNum.length() != 10) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Phone Number Input");
			alert.setContentText("Phone number entered incorrectly!");
			alert.showAndWait();
			return;
		}

		try {
			Tenant.addTenant(unitUsed.getUnitID(), nameInput.getText().trim(), emailInput.getText(), phoneNum);
			refresh();
		} catch (IllegalArgumentException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("One tenant allowed");
			alert.setContentText("You can't enter more than one tenant!");
			alert.showAndWait();
			return;
		}

		if (AdminUnitsController.getSwitchBackToUnits()) {
			App.setScene("admin_units");
		}

	}

	@FXML
	private void goToPayments() {
		App.setScene("admin_payment");
	}

	@FXML
	private void deleteSelectedRow() {
		Tenant.removeTenant(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		if (unitUsed == null)
			table.getItems().addAll(Tenant.getTenants());
		else
			table.getItems().addAll(Tenant.getTenant(unitUsed.getUnitID()));
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}