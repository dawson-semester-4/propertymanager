package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.w3c.dom.Text;
import propertymanager.backend.FinancialInstitution;
import propertymanager.backend.Lease;

public class AdminFinancialInstitutionController extends Controller {

	@FXML
	private CheckBox editable;
	@FXML
	private TableView<FinancialInstitution> table;
	@FXML
	private TableColumn<FinancialInstitution, String> institutionName;
	@FXML
	private TableColumn<FinancialInstitution, String> interestRate;

	@FXML
	private HBox addDetails;

	@FXML
	private VBox actionButtons;

	@FXML
	private TextField institutionNameInput;

	@FXML
	private TextField interestRateInput;


	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		institutionName.setCellValueFactory(new PropertyValueFactory<>("institutionName"));
		initColumn(institutionName);
		interestRate.setCellValueFactory(new PropertyValueFactory<>("interestRate"));
		initColumn(interestRate);

		table.getItems().addAll(FinancialInstitution.getFinancialInstitutions());
	}

	private void initColumn(TableColumn<FinancialInstitution, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getInstitutionID();
			FinancialInstitution.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	@FXML
	private void addInstitution() {
		float interestRateNum;
		try {
			interestRateNum = Float.parseFloat(interestRateInput.getText().trim().replaceAll("%$", ""));
		} catch (NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Input");
			alert.setContentText("Interest rate entered incorrectly!");
			alert.showAndWait();
			return;
		}
		FinancialInstitution.addInstitution(institutionNameInput.getText().trim(), interestRateNum);
		refresh();
	}

	@FXML
	private void deleteSelectedRow() {
		FinancialInstitution.removeFinancialInstitution(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		table.getItems().addAll(FinancialInstitution.getFinancialInstitutions());
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}
