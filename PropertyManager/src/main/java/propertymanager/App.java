package propertymanager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import propertymanager.backend.DBConnection;

public class App extends Application {

	//    private static Scene scene;
	private static Stage stageView;
	private static String lastScene = "";
	private static String currentScene = "login"; // also the starting scene

	public static String getLastScene() {
		return lastScene;
	}

	@Override
	public void start(Stage stage) throws IOException {
		stageView = stage;
		stageView.setResizable(false);
		stageView.setScene(new Scene(loadFXML(currentScene)));
		stageView.setTitle("Property Manager");
		stageView.show();
	}

	public static void setScene(String fxml) {
		// commit every time the scene changes
		DBConnection.commit();

		try {
			stageView.setScene(new Scene(loadFXML(fxml)));
			lastScene = currentScene;
			currentScene = fxml;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Parent loadFXML(String fxml) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
		return fxmlLoader.load();
	}

	public static void main(String[] args) {
		launch();

		// commit and close sql connection when finished
		try {
			Connection conn = DBConnection.getInstance().getConnection();
			conn.commit();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
