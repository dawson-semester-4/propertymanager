package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import propertymanager.backend.DBConnection;
import propertymanager.backend.Login;

import java.util.regex.Pattern;

public abstract class Controller {
	@FXML
	private Label userNameLabel;


	void setUserNameLabel() {
		userNameLabel.setText(userNameLabel.getText() + Login.getName());
	}

	String onlyDigits(String s) {
//        String noDigits = s.replaceAll("^\\d+", "");
//        return s.replaceAll(noDigits, "");
		return s.replaceAll("[^\\d].+", "");
	}

	boolean verifyEmail(String email) {
		Pattern emailPattern = Pattern.compile("\\b[\\w\\.-]+@[\\w\\.-]+\\.\\w{2,4}\\b", Pattern.CASE_INSENSITIVE);
		return emailPattern.matcher(email).find();
	}

	public String phoneNumOnlyDigits(String phone) {
		return phone.replaceAll("(\\+|\\.|\\s|-|\\(|\\)|[^0-9])", "");
	}

	@FXML
	void commit() {
		DBConnection.commit();
	}

	@FXML
	void rollback() {
		DBConnection.rollback();
		refresh();
	}

	abstract void refresh();

	@FXML
	void switchToPrevious() {
		App.setScene(App.getLastScene());
	}

	@FXML
	void switchToLogin() {
		App.setScene("login");
	}

	@FXML
	void switchToContractor() {
		App.setScene("admin_contractor");
	}

	@FXML
	void switchToFinancialInstitution() {
		App.setScene("admin_financialInstitution");
	}

	@FXML
	void switchToTenants() {
		App.setScene("admin_tenants");
	}

	@FXML
	void switchToLease() {
		App.setScene("admin_lease");
	}

	@FXML
	void switchToPayment() {
		App.setScene("admin_payment");
	}

	@FXML
	void switchToMaintenanceLog() {
		App.setScene("admin_maintenanceLog");
	}

	@FXML
	void switchToMortgage() {
		App.setScene("admin_mortgage");
	}

	@FXML
	void switchToProperty() {
		App.setScene("admin_property");
	}

	@FXML
	void switchToUnits() {
		App.setScene("admin_units");
	}
}