package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.*;

public class AdminMortgageController extends Controller {
	@FXML
	private CheckBox editable;
	@FXML
	private HBox addDetails;
	@FXML
	private VBox actionButtons;

	@FXML
	private TextField propertyAddressInput;

	@FXML
	private Label inputInfo;

	@FXML
	private TableView<Mortgage> table;
	@FXML
	private TableColumn<Mortgage, String> address;
	@FXML
	private TableColumn<Mortgage, String> institutionName;
	@FXML
	private TableColumn<Mortgage, String> price;
	@FXML
	private TableColumn<Mortgage, String> balance;
	@FXML
	private TableColumn<Mortgage, String> monthlyPayment;
	@FXML
	private TableColumn<Mortgage, String> interestRate;

	@FXML
	private ComboBox<String> institutionInput;
	@FXML
	private TextField priceInput;
	@FXML
	private TextField balanceInput;
	@FXML
	private TextField monthlyPaymentInput;
	@FXML
	private TextField interestRateInput;

	private static Property propertyUsed;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		address.setCellValueFactory(new PropertyValueFactory<>("address"));
		initColumn(address);
		institutionName.setCellValueFactory(new PropertyValueFactory<>("instName"));
		initColumn(institutionName);
		price.setCellValueFactory(new PropertyValueFactory<>("price"));
		initColumn(price);
		balance.setCellValueFactory(new PropertyValueFactory<>("balance"));
		initColumn(balance);
		monthlyPayment.setCellValueFactory(new PropertyValueFactory<>("monthlyPayment"));
		initColumn(monthlyPayment);
		interestRate.setCellValueFactory(new PropertyValueFactory<>("interestRate"));
		initColumn(interestRate);


		propertyAddressInput.textProperty().addListener((obs, oldText, newText) -> {
			propertyUsed = Property.getProperty(newText);
			inputInfo.setVisible(propertyUsed != null);
			refresh();
		});

		institutionInput.getItems().addAll(FinancialInstitution.getFinancialInstitutionNames());
		institutionInput.getSelectionModel().selectFirst();
	}


	private void initColumn(TableColumn<Mortgage, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getPropertyID();
			Mortgage.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	@FXML
	private void addMortgage() {
		float priceNum, balanceNum, monthlyPaymentNum, interestRateNum;
		try {
			priceNum = Float.parseFloat(priceInput.getText());
			balanceNum = Float.parseFloat(balanceInput.getText());
			monthlyPaymentNum = Float.parseFloat(monthlyPaymentInput.getText());
			interestRateNum = Float.parseFloat(interestRateInput.getText().trim().replaceAll("%$", ""));
		} catch (NumberFormatException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Input");
			alert.setContentText("Number(s) inputted incorrectly!");
			alert.showAndWait();
			return;
		}

		try {
			Mortgage.addMortgage(propertyUsed.getPropertyID(), institutionInput.getValue(), priceNum, balanceNum,
			                     monthlyPaymentNum, interestRateNum);
		} catch (NullPointerException e) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("No property selected");
			alert.setContentText("Enter an address to add a mortgage to it");
			alert.showAndWait();
			return;
		}
		refresh();
	}

	@FXML
	private void deleteSelectedRow() {
		Mortgage.removeMortgage(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		if (propertyUsed == null)
			table.getItems().addAll(Mortgage.getMortgages());
		else
			table.getItems().addAll(Mortgage.getMortgage(propertyUsed.getPropertyID()));
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}
