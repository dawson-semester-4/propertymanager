package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import propertymanager.backend.DBConnection;

public class AdminController extends Controller {

	@FXML
	private Label welcome;

	@FXML
	private void initialize() {
		welcome.setText(welcome.getText() + LoginController.getNameGiven());
	}

	@FXML
	private void resetDB() {
		DBConnection.manageTables(false); // drop tables
		DBConnection.manageTables(true); // create tables
		DBConnection.insertData();
	}

	void refresh() {
	}
}
