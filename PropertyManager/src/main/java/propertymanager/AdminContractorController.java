package propertymanager;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import propertymanager.backend.Contractor;

public class AdminContractorController extends Controller {

	@FXML
	private CheckBox editable;

	@FXML
	private VBox actionButtons;

	@FXML
	private HBox addDetails;

	@FXML
	private TableView<Contractor> table;

	@FXML
	private TableColumn<Contractor, String> contractorID;

	@FXML
	private TableColumn<Contractor, String> propertyAddress;

	@FXML
	private TableColumn<Contractor, String> contractorName;

	@FXML
	private TableColumn<Contractor, String> type;

	@FXML
	private TableColumn<Contractor, String> email;

	@FXML
	private TableColumn<Contractor, String> phone;

	@FXML
	private TextField addressInput;

	@FXML
	private TextField nameInput;

	@FXML
	private TextField typeInput;

	@FXML
	private TextField emailInput;

	@FXML
	private TextField phoneInput;

	@FXML
	private void initialize() {
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		setUserNameLabel();

		contractorID.setCellValueFactory(new PropertyValueFactory<>("contractorID"));
		initColumn(contractorID);
		propertyAddress.setCellValueFactory(new PropertyValueFactory<>("propertyAddress"));
		initColumn(propertyAddress);
		contractorName.setCellValueFactory(new PropertyValueFactory<>("name"));
		initColumn(contractorName);
		type.setCellValueFactory(new PropertyValueFactory<>("type"));
		initColumn(type);
		email.setCellValueFactory(new PropertyValueFactory<>("email"));
		initColumn(email);
		phone.setCellValueFactory(new PropertyValueFactory<>("phone"));
		initColumn(phone);

		table.getItems().addAll(Contractor.getContractors());
	}

	private void initColumn(TableColumn<Contractor, String> column) {
		column.setCellFactory(TextFieldTableCell.forTableColumn());
		column.setOnEditCommit(event -> {
			int recordIdentifier = event.getRowValue().getPropertyID();
			Contractor.update(event.getTableColumn().getText(), recordIdentifier, event.getNewValue());
		});
	}

	@FXML
	private void addContractor() {
		if (!verifyEmail(emailInput.getText())) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Email Input");
			alert.setContentText("Email entered incorrectly!");
			alert.showAndWait();
			return;
		}

		String phoneNum = phoneNumOnlyDigits(phoneInput.getText());
		if (phoneNum.length() != 10) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("Invalid Phone Input");
			alert.setContentText("Phone number entered incorrectly!");
			alert.showAndWait();
			return;
		}


		Contractor.addContractor(addressInput.getText().trim(), nameInput.getText().trim(), typeInput.getText().trim(),
		                         emailInput.getText(), phoneNum);
		refresh();
	}

	@FXML
	private void deleteSelectedRow() {
		Contractor.removeContractor(table.getSelectionModel().getSelectedItem());
		refresh();
	}

	void refresh() {
		table.getItems().clear();
		table.getItems().addAll(Contractor.getContractors());
	}

	@FXML
	private void setEditable() {
		table.setEditable(editable.isSelected());
		addDetails.setDisable(!editable.isSelected());
		actionButtons.setDisable(!editable.isSelected());
	}
}
