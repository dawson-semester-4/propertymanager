module propertymanager {
	requires java.sql;
	requires javafx.fxml;
	requires javafx.controls;

	opens propertymanager;
	exports propertymanager.backend;
	exports propertymanager;
}
