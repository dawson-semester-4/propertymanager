-- tables
-- Table: Financial_Institutions
CREATE TABLE Financial_Institutions
(
    fin_institution_id int           NOT NULL AUTO_INCREMENT,
    name               varchar(30)   NOT NULL,
    interest           decimal(6, 2) NULL,
    CONSTRAINT Financial_Institutions_pk PRIMARY KEY (fin_institution_id)
);

-- Table: Leases
CREATE TABLE Leases
(
    unit_id    int           NOT NULL,
    start_date date          NOT NULL,
    end_date   date          NOT NULL,
    rent       decimal(7, 2) NOT NULL,
    CONSTRAINT Leases_pk PRIMARY KEY (unit_id)
);

-- Table: Login
CREATE TABLE Login
(
    user_id       int          NOT NULL AUTO_INCREMENT,
    unit_id       int          NULL DEFAULT 0,
    admin_access  bool         NOT NULL,
    name          varchar(60)  NOT NULL,
    password_hash varchar(256) NOT NULL,
    salt          varchar(80)  NOT NULL,
    CONSTRAINT Login_pk PRIMARY KEY (user_id)
);

-- Table: Maintenance_contractors
CREATE TABLE Maintenance_contractors
(
    contractor_id int         NOT NULL AUTO_INCREMENT,
    property_id   int         NOT NULL,
    name          varchar(30) NOT NULL,
    type          varchar(30) NOT NULL,
    email         varchar(60) NOT NULL,
    phone         char(10)    NOT NULL,
    CONSTRAINT Maintenance_contractors_pk PRIMARY KEY (contractor_id)
);

-- Table: Maintenance_log
CREATE TABLE Maintenance_log
(
    unit_id       int          NOT NULL,
    contractor_id int          NOT NULL,
    date          date         NOT NULL,
    type          varchar(30)  NOT NULL,
    description   varchar(200) NOT NULL,
    CONSTRAINT Maintenance_log_pk PRIMARY KEY (unit_id, contractor_id)
);

-- Table: Mortgage
CREATE TABLE Mortgage
(
    property_id        int            NOT NULL,
    fin_institution_id int            NOT NULL,
    price              decimal(12, 2) NOT NULL,
    balance            decimal(12, 2) NOT NULL,
    monthly_payment    decimal(12, 2) NOT NULL,
    interest           decimal(6, 2)  NULL,
    CONSTRAINT Mortgage_pk PRIMARY KEY (property_id)
);

-- Table: Payment
CREATE TABLE Payment
(
    unit_id          int           NOT NULL,
    paid_flag        bool          NOT NULL,
    payment_due_date date          NOT NULL,
    date_paid        date          NULL,
    payment_method   varchar(20)   NULL,
    amount           decimal(8, 2) NULL,
    CONSTRAINT Payment_pk PRIMARY KEY (unit_id)
);

-- Table: Properties
CREATE TABLE Properties
(
    property_id int         NOT NULL AUTO_INCREMENT,
    type        varchar(30) NOT NULL,
    address     varchar(60) NOT NULL,
    unit_count  int         NOT NULL DEFAULT 1,
    CONSTRAINT Properties_pk PRIMARY KEY (property_id)
);

-- Table: Tenants
CREATE TABLE Tenants
(
    unit_id int         NOT NULL,
    name    varchar(30) NOT NULL,
    email   varchar(60) NOT NULL,
    phone   char(10)    NOT NULL,
    CONSTRAINT Tenants_pk PRIMARY KEY (unit_id)
);

-- Table: Units
CREATE TABLE Units
(
    unit_id      int           NOT NULL AUTO_INCREMENT,
    property_id  int           NOT NULL,
    apt_number   int           NULL,
    monthly_rent decimal(8, 2) NOT NULL,
    condo_fee    decimal(5, 2) NULL,
    CONSTRAINT Units_pk PRIMARY KEY (unit_id)
);

-- foreign keys
-- Reference: Financial_Institutions_Mortgage (table: Mortgage)
ALTER TABLE Mortgage
    ADD CONSTRAINT Financial_Institutions_Mortgage FOREIGN KEY Financial_Institutions_Mortgage (fin_institution_id)
        REFERENCES Financial_Institutions (fin_institution_id);

-- Reference: Leases_Units (table: Leases)
ALTER TABLE Leases
    ADD CONSTRAINT Leases_Units FOREIGN KEY Leases_Units (unit_id)
        REFERENCES Units (unit_id) ON DELETE CASCADE;

-- Reference: Login_Tenants (table: Login)
ALTER TABLE Login
    ADD CONSTRAINT Login_Tenants FOREIGN KEY Login_Tenants (unit_id)
        REFERENCES Tenants (unit_id) ON DELETE CASCADE;

-- Reference: Maintenance_Contractors (table: Maintenance_log)
ALTER TABLE Maintenance_log
    ADD CONSTRAINT Maintenance_Contractors FOREIGN KEY Maintenance_Contractors (contractor_id)
        REFERENCES Maintenance_contractors (contractor_id) ON DELETE CASCADE;

-- Reference: Maintenance_Units (table: Maintenance_log)
ALTER TABLE Maintenance_log
    ADD CONSTRAINT Maintenance_Units FOREIGN KEY Maintenance_Units (unit_id)
        REFERENCES Units (unit_id) ON DELETE CASCADE;

-- Reference: Payment_Tenants (table: Payment)
ALTER TABLE Payment
    ADD CONSTRAINT Payment_Tenants FOREIGN KEY Payment_Tenants (unit_id)
        REFERENCES Tenants (unit_id) ON DELETE CASCADE;

-- Reference: Properties_Maintenance_Contractors (table: Maintenance_contractors)
ALTER TABLE Maintenance_contractors
    ADD CONSTRAINT Properties_Maintenance_Contractors FOREIGN KEY Properties_Maintenance_Contractors (property_id)
        REFERENCES Properties (property_id) ON DELETE CASCADE;

-- Reference: Properties_Mortgage (table: Mortgage)
ALTER TABLE Mortgage
    ADD CONSTRAINT Properties_Mortgage FOREIGN KEY Properties_Mortgage (property_id)
        REFERENCES Properties (property_id) ON DELETE CASCADE;

-- Reference: Properties_Units (table: Units)
ALTER TABLE Units
    ADD CONSTRAINT Properties_Units FOREIGN KEY Properties_Units (property_id)
        REFERENCES Properties (property_id) ON DELETE CASCADE;

-- Reference: Units_Tenants (table: Tenants)
ALTER TABLE Tenants
    ADD CONSTRAINT Units_Tenants FOREIGN KEY Units_Tenants (unit_id)
        REFERENCES Units (unit_id) ON DELETE CASCADE;