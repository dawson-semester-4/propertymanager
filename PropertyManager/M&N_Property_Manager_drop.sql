-- foreign keys
ALTER TABLE Mortgage
    DROP FOREIGN KEY Financial_Institutions_Mortgage;

ALTER TABLE Leases
    DROP FOREIGN KEY Leases_Units;

ALTER TABLE Login
    DROP FOREIGN KEY Login_Tenants;

ALTER TABLE Maintenance_log
    DROP FOREIGN KEY Maintenance_Contractors;

ALTER TABLE Maintenance_log
    DROP FOREIGN KEY Maintenance_Units;

ALTER TABLE Payment
    DROP FOREIGN KEY Payment_Tenants;

ALTER TABLE Maintenance_contractors
    DROP FOREIGN KEY Properties_Maintenance_Contractors;

ALTER TABLE Mortgage
    DROP FOREIGN KEY Properties_Mortgage;

ALTER TABLE Units
    DROP FOREIGN KEY Properties_Units;

ALTER TABLE Tenants
    DROP FOREIGN KEY Units_Tenants;

-- tables
DROP TABLE Financial_Institutions;

DROP TABLE Leases;

DROP TABLE Login;

DROP TABLE Maintenance_contractors;

DROP TABLE Maintenance_log;

DROP TABLE Mortgage;

DROP TABLE Payment;

DROP TABLE Properties;

DROP TABLE Tenants;

DROP TABLE Units;

-- End of file.

